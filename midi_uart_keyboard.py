#!/usr/bin/env python3

import app
import atexit
import pygame, sys
import midi_uart
from app.flag import Flag
from app import Color, glob


def draw_frame():
  glob.screen.fill(Color.BLACK)
  glob.white_keys.draw(glob.screen)
  glob.black_keys.draw(glob.screen)
  glob.left_panel.draw(glob.screen)
  pygame.display.flip()

def generate_keys():
  white_key_labels = list(glob.white_shortcuts)
  note_number = 48
  horizontal_padding = 3
  white_keys_number = 21
  keyboard_width = glob.resolution[0] - glob.left_panel_width
  white_key_width = (keyboard_width // white_keys_number) - horizontal_padding
  left_padding = ((keyboard_width - (white_keys_number * (white_key_width + horizontal_padding)) + horizontal_padding) // 2) + glob.left_panel_width

  black_key_labels = list(glob.black_shortcuts)
  # height => 56% of white key width
  black_key_width = int((white_key_width + horizontal_padding) * 0.5)
  odd_pattern = True
  next_skipped_key = 2

  for i in range(white_keys_number):
    label = white_key_labels.pop(0) if len(white_key_labels) > 0 else None
    white_key = app.sprite.KeyboardKey(white_key_width, glob.resolution[1], left_padding + i * (white_key_width + horizontal_padding), shortcut=label, note=note_number)
    glob.white_keys.add(white_key)
    glob.white_shortcuts[label] = white_key
    note_number += 1

    # height => 64% of window height
    if next_skipped_key != i:
      label = black_key_labels.pop(0)
      # int(left_padding + (1.5 + i * 2) * black_key_width)
      black_key = app.sprite.KeyboardKey(black_key_width, int(glob.resolution[1] * 0.64),  left_padding + white_key_width - (black_key_width) // 2 - horizontal_padding // 2 + i * (white_key_width + horizontal_padding), black=True, shortcut=label, note=note_number)
      glob.black_keys.add(black_key)
      glob.black_shortcuts[label] = black_key
      note_number += 1
      continue

    if odd_pattern:
      next_skipped_key += 4
      odd_pattern = False
    else:
      next_skipped_key += 3
      odd_pattern = True

def generate_slider(label, slider_knob_class, gauge_class, active=False):
  slider_slot_width = int(0.014 * glob.resolution[0])
  slider_slot_height = int(0.8 * glob.resolution[1])
  x_offset = glob.current_slider_index * glob.slider_wrapper_width
  slider_slot_x = x_offset + glob.slider_wrapper_width // 2 - slider_slot_width // 2
  slider_width = glob.slider_wrapper_width * 0.6
  slider_height = int(0.085 * glob.resolution[1])
  slider_min_y = slider_slot_y = (glob.resolution[1] - slider_slot_height) // 2
  slider_max_y = glob.resolution[1] - slider_slot_y - slider_height

  slider_gauge_y = slider_slot_y // 2 - slider_height // 2
  slider_label_y = glob.resolution[1] - slider_slot_y // 2 - slider_height // 2
  slider_x = x_offset + (glob.slider_wrapper_width - slider_width) // 2

  glob.left_panel.add(app.sprite.slider.BaseSlot(slider_slot_width, slider_slot_height, slider_slot_x, slider_slot_y))
  gauge = gauge_class(slider_width, slider_height, slider_x, slider_gauge_y)
  knob = slider_knob_class(slider_width, slider_height, slider_x, slider_min_y, slider_max_y, gauge=gauge, active=active)
  glob.left_panel.add(knob)
  glob.left_panel.add(gauge)
  glob.left_panel.add(app.sprite.slider.BaseLabel(label, slider_width, slider_height, slider_x, slider_label_y))
  glob.current_slider_index += 1
  return knob


def generate_sliders():
  glob.left_panel.add(app.sprite.LeftPanelBackground(glob.left_panel_width, glob.resolution[1]))

  # Pitch slider
  glob.slider_knobs['pitch'] = generate_slider('PTCH', app.sprite.slider.PitchKnob, app.sprite.slider.PitchGauge, active=True)
  # Modulation slider
  glob.slider_knobs['mod'] = generate_slider('MOD', app.sprite.slider.ModKnob, app.sprite.slider.ModGauge)

def generate_objects():
  glob.initialize()
  generate_sliders()
  generate_keys()

def handle_key_event(key, type = 'down'):
  if not key:
    return
  input_character = key.lower()
  white_key = glob.white_shortcuts.get(input_character)
  black_key = glob.black_shortcuts.get(input_character)
  if white_key:
    white_key.press() if type == 'down' else white_key.release()
  elif black_key:
    black_key.press() if type == 'down' else black_key.release()

def main():
  atexit.register(midi_uart.exit_handler)
  pygame.init()
  pygame.font.init()

  if glob.pygame_main_version == 1: # Pygame < 2 compatibility
    glob.screen = pygame.display.set_mode(glob.resolution, pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE)
  else:
    glob.screen = pygame.display.set_mode(glob.resolution, pygame.RESIZABLE)
  glob.clock = pygame.time.Clock()
  glob.serial = midi_uart.open_serial()
  midi_uart.print_connection_info()

  pygame.display.set_caption("MIDI UART Keyboard")
  run = True
  generate_objects()

  while run:
    event_list = pygame.event.get()
    for event in event_list:
      if event.type == pygame.QUIT:
        run = False
        break
      if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_ESCAPE:
          # press Escape to enter debugging mode
          breakpoint()
        else:
          handle_key_event(event.unicode, 'down')
      elif event.type == pygame.KEYUP:
        active_key = ''
        try:
          active_key = event.unicode
        except AttributeError: # Pygame < 2 compatibility
          active_key = chr(event.key)

        handle_key_event(active_key, 'up')
      elif event.type == pygame.VIDEORESIZE:
        if glob.pygame_main_version == 1: # Pygame < 2 compatibility
          glob.resolution = event.dict['size']
          glob.screen = pygame.display.set_mode(glob.resolution, pygame.HWSURFACE|pygame.DOUBLEBUF|pygame.RESIZABLE) # Width and Height of the window
          generate_objects()
        else:
          new_resolution = glob.screen.get_size()
          if new_resolution[0] < 700 or new_resolution[1] < 200:
            glob.screen = pygame.display.set_mode(glob.resolution, pygame.RESIZABLE)
          else:
            glob.resolution = glob.screen.get_size()
            generate_objects()

    key_found = Flag(False)
    glob.black_keys.update(event_list, key_found)
    if not key_found:
      glob.white_keys.update(event_list, key_found)
    glob.left_panel.update(event_list)
    draw_frame()
    glob.clock.tick(120) # 120 FPS at max

  pygame.quit()
  sys.exit()

# only run this code when this is the main file (the one that was passed to the interpreter)
if __name__ == "__main__":
  main()
