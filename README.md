# Virtual MIDI keyboard

A simple Virtual on-screen keyboard which sends MIDI messages through UART or USART

[[_TOC_]]

## Description
This is a bundle of 2 related programmes.

- `midi_uart.py` - CLI program for building and sending MIDI messages through UART\USART
- `midi_uart_keyboard.py` - graphical on-screen MIDI keyboard with shortcuts for every key which also sends MIDI messages through UART/USART

## Visuals
### Keyboard
![MIDI UART Keyboard Gif](assets/midi_uart_keyboard_demo.gif)

### CLI program
![MIDI UART CLI Gif](assets/midi_uart_demo.gif)

## Installation
This programme works best on Python >= 3.10 and Pygame >= 2.
Although it is compatible with Pygame 1.

1. Clone this repository

```sh
$ git clone https://gitlab.com/mateuszdrewniak/virtual-midi-keyboard.git
```

2. Enter the previously cloned repo

```sh
$ cd virtual-midi-keyboard
```

3. Install dependencies

```sh
$ pip install -r requirements.txt
```

## Usage

Both programmes share configuration options. They can be modified using environment variables.

### MIDI UART CLI

![](assets/midi_uart_screen_small.png)

Here's how to run the CLI programme.
```sh
$ PORT_NAME=<uart_port_name> BAUDRATE=<baudrate> python midi_uart.py
```

#### Linux

The `PORT_NAME` will most likely resemble `/dev/ttyACM0`.
To find it you should do `ls -a /dev` before and after plugging in your device (microcontroller) and see which item has been added.

The `BAUDRATE` value depends on your device's (microcontroller's) settings.
```sh
$ PORT_NAME=/dev/ttyACM0 BAUDRATE=115200 python midi_uart.py
```

#### MacOS
The `PORT_NAME` will most likely resemble `/dev/tty.usbmodem103`.
To find it you should do `ls -a /dev` before and after plugging in your device (microcontroller) and see which item has been added.

The `BAUDRATE` value depends on your device's (microcontroller's) settings.
```sh
$ PORT_NAME=/dev/tty.usbmodem103 BAUDRATE=115200 python midi_uart.py
```

### MIDI UART Virtual Keyboard

![](assets/midi_uart_keyboard_screen_small.png)

The pitch bend and modulation sliders are controlled by your
mouse's scroll wheel.

Only one slider is controlled at once. To change the active slider
just click on the other slider's knob.

Here's how to run the graphical programme.
```sh
$ PORT_NAME=<uart_port_name> BAUDRATE=<baudrate> python midi_uart_keyboard.py
```

#### Linux

The `PORT_NAME` will most likely resemble `/dev/ttyACM0`.
To find it you should do `ls -a /dev` before and after plugging in your device (microcontroller) and see which item has been added.

The `BAUDRATE` value depends on your device's (microcontroller's) settings.
```sh
$ PORT_NAME=/dev/ttyACM0 BAUDRATE=115200 python midi_uart_keyboard.py
```

#### MacOS
The `PORT_NAME` will most likely resemble `/dev/tty.usbmodem103`.
To find it you should do `ls -a /dev` before and after plugging in your device (microcontroller) and see which item has been added.

The `BAUDRATE` value depends on your device's (microcontroller's) settings.
```sh
$ PORT_NAME=/dev/tty.usbmodem103 BAUDRATE=115200 python midi_uart_keyboard.py
```

## MacOS problems

If installing `pygame` fails, you should try installing the following
libraries:

```sh
$ brew install sdl sdl_image sdl_mixer sdl_ttf portmidi
$ brew install sdl2 sdl2_image sdl2_mixer sdl2_ttf pkg-config
```
