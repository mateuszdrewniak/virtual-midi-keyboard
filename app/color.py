class Color:
  WHITE = (249, 248, 248)
  BLACK = (17, 17, 24)
  SOFT_BLACK = (39, 40, 56)
  RED = (242, 95, 92)
  VIVID_RED = (237, 32, 29)
  GRAY = (169, 180, 194)
