import pygame

pygame_main_version = pygame.version.vernum[0]
resolution = [1465, 350]
SLIDER_COUNT = 2
slider_wrapper_width = None
left_panel_width = None
shortcut_font = None
note_font = None
current_slider_index = None
screen = None
clock = None
serial = None
PITCH_RANGE = range(-8160, 8161)
MOD_RANGE = range(0, 128)
PITCH_SLIDER_SCROLL_MULTIPLIER = 80

NOTE_DICT = {
  83:  'B5',
  82:  'A#5',
  81:  'A5',
  80:  'G#5',
  79:  'G5',
  78:  'F#5',
  77:  'F5',
  76:  'E5',
  75:  'D#5',
  74:  'D5',
  73:  'C#5',
  72:  'C5',
  71:  'B4',
  70:  'A#4',
  69:  'A4',
  68:  'G#4',
  67:  'G4',
  66:  'F#4',
  65:  'F4',
  64:  'E4',
  63:  'D#4',
  62:  'D4',
  61:  'C#4',
  60:  'C4',
  59:  'B3',
  58:  'A#3',
  57:  'A3',
  56:  'G#3',
  55:  'G3',
  54:  'F#3',
  53:  'F3',
  52:  'E3',
  51:  'D#3',
  50:  'D3',
  49:  'C#3',
  48:  'C3'
}

def initialize():
  global current_slider_index, white_shortcuts, black_shortcuts, slider_knobs, white_keys, black_keys, left_panel, shortcut_font, note_font, slider_wrapper_width, left_panel_width
  slider_wrapper_width = int(resolution[0] * 0.055)
  left_panel_width = slider_wrapper_width * SLIDER_COUNT
  shortcut_font = pygame.font.SysFont('Arial', int(resolution[0] * 0.016))
  note_font = pygame.font.SysFont('Arial', int(resolution[0] * 0.0115))
  current_slider_index = 0
  white_shortcuts = dict.fromkeys(['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', 'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '/'])
  black_shortcuts = dict.fromkeys(['2', '3', '5', '6', '7', '9', '0', 's', 'd', 'f', 'h', 'j', 'l', ';', "'"])
  slider_knobs = dict.fromkeys(['pitch', 'mod'])
  white_keys = pygame.sprite.Group()
  black_keys = pygame.sprite.Group()
  left_panel = pygame.sprite.Group()
