class Flag:
  def __init__(self, value):
    self.value = value

  def set(self):
    self.value = True

  def unset(self):
    self.value = False

  def __bool__(self):
    return bool(self.value)
