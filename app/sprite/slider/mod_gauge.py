import midi_uart
import app
from .base_gauge import BaseGauge

class ModGauge(BaseGauge):
  def send_midi_message(self):
    msg = midi_uart.build_midi_message('control_change', control=1, value=self.value, channel=0)
    midi_uart.send_midi_message(app.glob.serial, msg)
    midi_uart.print_message(msg)
