import app
import midi_uart
from .base_gauge import BaseGauge

class PitchGauge(BaseGauge):
  def send_midi_message(self):
    msg = midi_uart.build_midi_message('pitch_bend', pitch=self.value, channel=0)
    midi_uart.send_midi_message(app.glob.serial, msg)
    midi_uart.print_message(msg)
