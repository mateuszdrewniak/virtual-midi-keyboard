import pygame
import app

class BaseLabel(pygame.sprite.Sprite):
  def __init__(self, text, width, height, pos_x = 0, pos_y = 0) -> None:
    super().__init__()

    self.image = pygame.Surface((width, height))
    self.image.fill(app.Color.SOFT_BLACK)
    self.rect = self.image.get_rect()
    self.rect.topleft = (pos_x, pos_y)

    text_surf = app.glob.note_font.render(text.upper(), True, app.Color.WHITE)
    self.image.blit(text_surf, (0, 0))
