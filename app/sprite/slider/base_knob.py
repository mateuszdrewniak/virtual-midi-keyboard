import pygame
import app

class BaseKnob(pygame.sprite.Sprite):
  value_multiplier = 1
  value_range = range(0, 127)

  def __init__(self, width, height, pos_x = 0, min_y = 0, max_y = 0, gauge = None, active = False) -> None:
    super().__init__()

    self.min_y = min_y
    self.max_y = max_y
    self.pos_x = pos_x
    self.gauge = gauge
    self.value = 0

    self.image = pygame.Surface((width, height))
    self.active = None
    self.activate() if active else self.deactivate()
    self.rect = self.image.get_rect()
    self.update_y()

  def scroll_to_value(self, scroll_y):
    value_change = scroll_y * self.value_multiplier
    if self.value == self.value_range[-1] and value_change > 0 or self.value == self.value_range[0] and value_change < 0:
      return

    if self.value + value_change > self.value_range[-1]:
      return self.value_range[-1]
    if self.value + value_change < self.value_range[0]:
      return self.value_range[0]

    return self.value + value_change

  def set_value(self, scroll_y):
    new_value = self.scroll_to_value(scroll_y)
    if new_value is None:
      return

    self.value = new_value
    self.update_y()
    if self.gauge:
      self.gauge.set_value(self.value)

  def activate(self):
    self.active = True
    self.image.fill(app.Color.RED)

  def deactivate(self):
    self.active = False
    self.image.fill(app.Color.GRAY)

  def update_y(self):
    base_scale = self.max_y - self.min_y
    positive_value = self.value - self.value_range[0]
    max_value = self.value_range[-1] - self.value_range[0]

    value_percentage = positive_value / max_value
    pos_x = base_scale - int(value_percentage * base_scale) + self.min_y
    self.rect.topleft = (self.pos_x, pos_x)

  def update(self, event_list):
    for event in event_list:
      if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and self.rect.collidepoint(event.pos):
        for knob in app.glob.slider_knobs.values():
          if knob is self:
            continue
          knob.deactivate()
        self.activate()
      elif self.active and app.glob.pygame_main_version >= 2 and event.type == pygame.MOUSEWHEEL: # pygame > 2 Mouse wheel
        self.set_value(event.y)
      elif self.active and app.glob.pygame_main_version < 2 and event.type == pygame.MOUSEBUTTONDOWN and event.button == 4: # pygame < 2 Mouse wheel up
        self.set_value(2)
      elif self.active and app.glob.pygame_main_version < 2 and event.type == pygame.MOUSEBUTTONDOWN and event.button == 5: # pygame < 2 Mouse wheel down
        self.set_value(-2)
