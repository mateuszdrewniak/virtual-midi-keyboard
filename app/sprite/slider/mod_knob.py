import app.glob
from .base_knob import BaseKnob

class ModKnob(BaseKnob):
  value_range = app.glob.MOD_RANGE
