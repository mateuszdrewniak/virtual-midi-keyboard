import pygame
import app

class BaseGauge(pygame.sprite.Sprite):
  def __init__(self, width, height, pos_x = 0, pos_y = 0) -> None:
    super().__init__()

    self.value = 0

    self.image = pygame.Surface((width, height))
    self.rect = self.image.get_rect()
    self.rect.topleft = (pos_x, pos_y)
    self.render_digits()

  def set_value(self, new_value):
    if new_value is None:
      return
    self.value = new_value
    self.render_digits()
    self.send_midi_message()

  def send_midi_message(self):
    return

  def render_digits(self):
    self.image.fill(app.Color.SOFT_BLACK)
    text_surf = app.glob.note_font.render(str(self.value), True, app.Color.WHITE)
    x_offset = (self.image.get_width() - text_surf.get_width()) // 2
    self.image.blit(text_surf, (x_offset, 0))
