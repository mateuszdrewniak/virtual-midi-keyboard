import app.glob
from .base_knob import BaseKnob

class PitchKnob(BaseKnob):
  value_multiplier = app.glob.PITCH_SLIDER_SCROLL_MULTIPLIER
  value_range = app.glob.PITCH_RANGE
