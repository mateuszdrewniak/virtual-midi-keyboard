import pygame
import midi_uart
import app

class KeyboardKey(pygame.sprite.Sprite):
  def __init__(self, width, height, pos_x, pos_y = 0, black = False, shortcut = None, note = None) -> None:
    super().__init__()

    self.note = note
    self.black = black
    self.shortcut = shortcut
    self.image = pygame.Surface((width, height))
    self.image.fill(app.Color.WHITE if self.is_white() else app.Color.SOFT_BLACK)
    self.shortcut_text_surf = None
    self.note_text_surf = None
    if shortcut:
      self.shortcut_text_surf = app.glob.shortcut_font.render(shortcut.upper(), True, app.Color.SOFT_BLACK if self.is_white() else app.Color.WHITE)
      self.render_label()
    if note:
      self.note_text_surf = app.glob.note_font.render(app.glob.NOTE_DICT.get(note).upper(), True, app.Color.SOFT_BLACK if self.is_white() else app.Color.WHITE)
      self.render_label()
    self.rect = self.image.get_rect()
    self.rect.topleft = (pos_x, pos_y)
    self.pressed = False

  def is_black(self):
    return self.black

  def is_white(self):
    return not self.black

  def is_pressed(self):
    return self.pressed

  def is_released(self):
    return not self.pressed

  def press(self):
    if self.is_pressed():
      return
    self.pressed = True
    if self.is_white():
      self.image.fill(app.Color.RED)
      self.render_label()
    else:
      self.image.fill(app.Color.VIVID_RED)
      self.render_label()

    msg = midi_uart.build_midi_message('note_on', note=self.note, velocity=64, channel=0)
    midi_uart.send_midi_message(app.glob.serial, msg)
    midi_uart.print_message(msg)

  def release(self):
    if self.is_released():
      return
    self.pressed = False
    self.image.fill(app.Color.WHITE if self.is_white() else app.Color.SOFT_BLACK)
    self.render_label()

    msg = midi_uart.build_midi_message('note_off', note=self.note, velocity=0, channel=0)
    midi_uart.send_midi_message(app.glob.serial, msg)
    midi_uart.print_message(msg)

  def render_label(self):
    if self.shortcut_text_surf:
      x_offset = (self.image.get_width() - self.shortcut_text_surf.get_width()) // 2
      y_offset = 25 if self.is_white() else 5
      self.image.blit(self.shortcut_text_surf, (x_offset, y_offset))
    if self.note_text_surf:
      x_offset = (self.image.get_width() - self.note_text_surf.get_width()) // 2
      y_offset = self.image.get_height() - self.note_text_surf.get_height() - 5
      self.image.blit(self.note_text_surf, (x_offset, y_offset))

  def update(self, event_list, key_found):
    for event in event_list:
      if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1 and self.rect.collidepoint(event.pos):
        self.press()
        key_found.set()
      elif event.type == pygame.MOUSEBUTTONUP and event.button == 1:
        self.release()
