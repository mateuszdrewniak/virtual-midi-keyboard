#!/usr/bin/env python3

import os
import atexit
from serial import Serial, serialutil
from mido import Message

# Windows compatibility
try:
  import readline
except ModuleNotFoundError:
  pass

class TerminalColors:
  BLUE = '\033[94m'
  CYAN = '\033[96m'
  GREEN = '\033[92m'
  WARNING = '\033[93m'
  FAIL = '\033[91m'
  END = '\033[0m'
  BOLD = '\033[1m'
  ITALIC = '\033[3m'
  UNDERLINE = '\033[4m'

def exit_handler():
  print(end=TerminalColors.END)

def get_port_name():
  return os.getenv('PORT_NAME') or '/dev/ttyACM0'

def get_baudrate():
  return os.getenv('BAUDRATE') or '115200'

def get_debug():
  return not not os.getenv('DEBUG')

def print_connection_info():
  port_name = get_port_name()
  baudrate = get_baudrate()
  separator_line = f"+{'-' * (34 + len(port_name) + len(baudrate))}+"

  print(f'''{separator_line}
| Connected to {TerminalColors.GREEN}`{port_name}`{TerminalColors.END} with baudrate {TerminalColors.GREEN}`{baudrate}`{TerminalColors.END} |
{separator_line}\n''')

def print_help():
  print(f'''syntax:
  For `note_on`, `note_off` messages:
    [command] [note] [velocity] [channel]

  For `pitch_bend`, `pb` messages:
    [command] [pitch] [channel]

  For `control_change`, `cc` messages:
    [command] [control] [value] [channel]

  {TerminalColors.ITALIC}command{TerminalColors.END}
    Optional. Defaults to `note_on`. Possible values:
      * `note_on` - send a `NOTE ON` MIDI message
      * `on` - send a `NOTE ON` MIDI message
      * `note_off` - send a `NOTE OFF` MIDI message
      * `off` - send a `NOTE OFF` MIDI message
      * `pitch_bend` - send a `PITCH BEND` MIDI message
      * `pb` - send a `PITCH BEND` MIDI message
      * `control_change` - send a `CONTROL CHANGE` MIDI message
      * `cc` - send a `CONTROL CHANGE` MIDI message
      * `help` - print help
      * `exit` - exit the programme
      * `quit` - exit the programme
  {TerminalColors.ITALIC}note{TerminalColors.END}
    Optional. Integer (0..127). Defaults to `60` (C4 note)
  {TerminalColors.ITALIC}velocity{TerminalColors.END}
    Optional. Integer (0..127). Defaults to `64` (Mezzo forte)
  {TerminalColors.ITALIC}pitch{TerminalColors.END}
    Optional. Integer (-8192..8191). Defaults to `0` (No pitch bend)
  {TerminalColors.ITALIC}channel{TerminalColors.END}
    Optional. Integer (0..15). Defaults to `0` (channel 1)
  {TerminalColors.ITALIC}value{TerminalColors.END}
    Optional. Integer (0..127). Defaults to `0` (no value change)
  {TerminalColors.ITALIC}control{TerminalColors.END}
    Optional. Integer (0..127). Defaults to `1` (modulation wheel)

  Optional parameters may be set to `.` to retain their default value.\n''')

def parse_command(command):
  command = command.split()
  # breakpoint()
  msg_type = command[0] if len(command) > 0 and command[0] != '.' else 'note_on'

  if msg_type in { '', 'note_on', 'note_off', 'on', 'off' }:
    note = int(command[1]) if len(command) > 1 and command[1] != '.' else 60
    velocity = int(command[2]) if len(command) > 2 and command[2] != '.' else 64
    channel = int(command[3]) if len(command) > 3 and command[3] != '.' else 0

    if note not in range(0, 128):
      print(f'`note` {note} is out of range\n')
      return
    if velocity not in range(0, 128):
      print(f'`velocity` {note} is out of range\n')
      return
    if channel not in range(0, 16):
      print(f'`channel` {note} is out of range\n')
      return

    return build_midi_message(msg_type, note=note, velocity=velocity, channel=channel)

  if msg_type in { 'pb', 'pitch_bend', 'pitchwheel' }:
    pitch = int(command[1]) if len(command) > 1 and command[1] != '.' else 0
    channel = int(command[2]) if len(command) > 2 and command[2] != '.' else 0

    if pitch not in range(-8192, 8192):
      print(f'`pitch` {pitch} is out of range\n')
      return
    if channel not in range(0, 16):
      print(f'`channel` {channel} is out of range\n')
      return
    return build_midi_message(msg_type, pitch=pitch, channel=channel)

  if msg_type in { 'control_change', 'cc' }:
    control = int(command[1]) if len(command) > 1 and command[1] != '.' else 1
    value = int(command[2]) if len(command) > 2 and command[2] != '.' else 0
    channel = int(command[3]) if len(command) > 3 and command[3] != '.' else 0

    if control not in range(0, 128):
      print(f'`control` {control} is out of range\n')
      return
    if value not in range(0, 128):
      print(f'`value` {value} is out of range\n')
      return
    if channel not in range(0, 16):
      print(f'`channel` {channel} is out of range\n')
      return
    return build_midi_message(msg_type, control=control, value=value, channel=channel)

  print('No such command!\n')

def build_midi_message(msg_type, note=0, velocity=64, channel=0, pitch=0, value=0, control=1):
  if msg_type in { 'pitch_bend', 'pb' }:
    return Message('pitchwheel', pitch=pitch, channel=channel)
  elif msg_type in { '', 'note_off', 'note_on' }:
    return Message(msg_type, note=note, velocity=velocity, channel=channel)
  elif msg_type in { 'on' }:
    return Message('note_on', note=note, velocity=velocity, channel=channel)
  elif msg_type in { 'off' }:
    return Message('note_off', note=note, velocity=velocity, channel=channel)
  elif msg_type in { 'control_change', 'cc' }:
    return Message('control_change', control=control, value=value, channel=channel)

def send_midi_message(serial, msg):
  if get_debug():
    return
  if type(msg) != Message:
    return False
  msg_bytes = bytes(msg.bytes())
  serial.write(msg_bytes)

  return True

def print_message(msg):
  msg_bytes = bytes(msg.bytes())
  msg_hex = ''.join('0x{:02x} '.format(x) for x in msg_bytes).strip()
  print(f'message:  {TerminalColors.GREEN}{msg}{TerminalColors.END}')
  print(f'ascii:    {TerminalColors.GREEN}{msg_bytes}{TerminalColors.END}')
  print(f'hex:      {TerminalColors.GREEN}{msg_hex}{TerminalColors.END}')
  print()

def process_input(serial):
  while True:
    print(end=TerminalColors.END)
    print(f'midi_cmd${TerminalColors.CYAN}{TerminalColors.BOLD}')

    command = input().strip().lower()
    print(end=TerminalColors.END)

    if command in { 'q', ':q', 'quit', 'exit' }:
      serial.close() if serial else None
      exit()

    if command in { 'help', 'h' }:
      print_help()
      continue

    msg = parse_command(command)

    if not msg:
      continue

    send_midi_message(serial, msg)
    print_message(msg)

def open_serial(port_name = None, baudrate = None):
  if get_debug():
    return
  if port_name is None:
    port_name = get_port_name()
  if baudrate is None:
    baudrate = get_baudrate()
  try:
    return Serial(port_name, baudrate)
  except serialutil.SerialException:
    print(f'No such port: {TerminalColors.FAIL}`{port_name}`{TerminalColors.END}')
    exit()

if __name__ == "__main__":
  atexit.register(exit_handler)
  ser = open_serial()

  print_help()
  print_connection_info()
  print()
  process_input(ser)
